def test_imports():
    import impd
    return None


def test_time():
    import impd.timing

    seconds = impd.timing.parse_time(100)
    assert(seconds == '01:40')
    return None


def test_client():
    import impd.client

    client = impd.client.Client
    assert(hasattr(client, 'connect'))
    assert(hasattr(client, 'currentsong'))
    assert(hasattr(client, 'pause'))
    assert(hasattr(client, 'next'))
    assert(hasattr(client, 'previous'))
    assert(hasattr(client, 'play'))
    assert(hasattr(client, 'playlistinfo'))
    return None


def test_musicbrainz():
    import impd.cover
    cover = impd.cover._musicbrainz_cover(
        'Plans', 'Birds Of Tokyo', 'Birds Of Tokyo')
    assert(cover is not None)
    return None
